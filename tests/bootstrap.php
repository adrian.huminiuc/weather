<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

$baseCommand = sprintf('APP_ENV=test php "%s/../bin/console"', __DIR__);

passthru($baseCommand . ' d:d:d --force --no-interaction');
passthru($baseCommand . ' d:d:create --no-interaction');
passthru($baseCommand . ' d:s:create --no-interaction');
passthru($baseCommand . ' d:f:load --no-interaction');
