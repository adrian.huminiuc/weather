<?php


namespace App\Tests\functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ForecastCrudApiControllerTest extends WebTestCase
{
    public function testCreate(): void
    {
        $client = $this->createClient();
        $client->xmlHttpRequest(
            'POST',
            '/forecast',
            [],
            [],
            [],
            json_encode(['city' => 'Berlin', 'date' => '2019-01-01 09:00:00', 'temp' => 6.89])
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
    }

    public function testCreateWithInvalidBody(): void
    {
        $client = $this->createClient();
        $client->xmlHttpRequest('POST', '/forecast', [], [], [], json_encode([]));

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent());
    }
}
