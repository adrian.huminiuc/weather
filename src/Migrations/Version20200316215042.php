<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200316215042 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add cities schema and some fixture cities';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('CREATE TABLE cities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, state VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, population INT NOT NULL, INDEX search_large_cities (population, country), UNIQUE INDEX unique_cities (name, state, country), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forecast (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, date DATETIME NOT NULL, temperature NUMERIC(10, 0) NOT NULL, INDEX IDX_2A9C78448BAC62AF (city_id), UNIQUE INDEX city_date (city_id, date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE forecast ADD CONSTRAINT FK_2A9C78448BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE forecast DROP FOREIGN KEY FK_2A9C78448BAC62AF');
        $this->addSql('DROP TABLE cities');
        $this->addSql('DROP TABLE forecast');
    }
}
