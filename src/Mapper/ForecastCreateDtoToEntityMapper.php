<?php


namespace App\Mapper;


use App\Dto\Request\CreateForecastDto;
use App\Entity\Forecast;
use App\Repository\ForecastCitiesRepository;

class ForecastCreateDtoToEntityMapper
{
    /**
     * @var ForecastCitiesRepository
     */
    private $citiesRepository;

    public function __construct(ForecastCitiesRepository $citiesRepository)
    {
        $this->citiesRepository = $citiesRepository;
    }

    public function map(CreateForecastDto $dto): Forecast
    {
        $city = $this->citiesRepository->findOneBy(['name' => $dto->getCity()]);

        return (new Forecast())
            ->setCity($city)
            ->setTemperature($dto->getTemp())
            ->setDate(\DateTime::createFromFormat('Y-m-d H:i:s', $dto->getDate()));
    }
}
