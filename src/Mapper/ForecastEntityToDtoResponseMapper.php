<?php


namespace App\Mapper;


use App\Dto\Response\ForecastDto;
use App\Entity\Forecast;

class ForecastEntityToDtoResponseMapper
{
    public function map(Forecast $forecast): ForecastDto
    {
        return new ForecastDto(
            $forecast->getId(),
            $forecast->getCity()->getName(),
            $forecast->getTemperature(),
            $forecast->getDate()
        );
    }
}
