<?php


namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class OpenWeatherClient
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getWeatherResponse(string $city): ResponseInterface
    {
        return $this->httpClient->request(
            'GET',
            'forecast',
            [
                'query' => [
                    'q' => sprintf('%s, Germany', $city),
                    'units' => 'metric',
                ]
            ]
        );
    }
}
