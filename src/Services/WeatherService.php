<?php


namespace App\Services;


use App\Factory\OpenWeatherToForecastFactory;
use App\Repository\ForecastCitiesRepository;
use App\Repository\ForecastRepository;

class WeatherService
{
    /**
     * @var OpenWeatherClient
     */
    private $openWeatherClient;

    /**
     * @var ForecastCitiesRepository
     */
    private $forecastCitiesRepository;

    /**
     * @var OpenWeatherToForecastFactory
     */
    private $openWeatherToForecastFactory;

    /**
     * @var ForecastRepository
     */
    private $forecastRepository;

    public function __construct(
        OpenWeatherClient $openWeatherClient,
        ForecastCitiesRepository $forecastCitiesRepository,
        OpenWeatherToForecastFactory $openWeatherToForecastFactory,
        ForecastRepository $forecastRepository
    ) {
        $this->openWeatherClient = $openWeatherClient;
        $this->forecastCitiesRepository = $forecastCitiesRepository;
        $this->openWeatherToForecastFactory = $openWeatherToForecastFactory;
        $this->forecastRepository = $forecastRepository;
    }

    public function fetch(string $city)
    {
        if (!$cityEntity = $this->forecastCitiesRepository->findOneBy(['name' => $city])) {
            throw new \InvalidArgumentException(sprintf('City %s does not exists', $city));
        }

        $fourDaysFromNow = new \DateTime('+4 days midnight');

        if (!$this->forecastRepository->findOneBy(['city' => $cityEntity, 'date' => $fourDaysFromNow])) {
            $response = $this->openWeatherClient->getWeatherResponse($city);
            if ($response->getStatusCode() !== 200) {
                throw new \InvalidArgumentException(sprintf('Cannot import data for city %s', $city));
            }

            $this->forecastRepository->persistBulk(
                $this->openWeatherToForecastFactory->createFromResponse(
                    json_decode($response->getContent(), true)
                )
            );
        }

        return $this->forecastRepository->getAverageThreeDayTemperature($cityEntity, $fourDaysFromNow);
    }
}
