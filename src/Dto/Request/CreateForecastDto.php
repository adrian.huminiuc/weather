<?php


namespace App\Dto\Request;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class CreateForecastDto
{
    /**
     * @JMS\Type("string")
     * @Assert\NotBlank(message="City cannot be empty")
     *
     * @var string
     */
    private $city;

    /**
     * @JMS\Type("double")
     * @Assert\NotBlank(message="Temperature cannot be empty")
     *
     * @var double
     */
    private $temp;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank(message="Date cannot be empty")
     *
     * @var string
     */
    private $date;

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): CreateForecastDto
    {
        $this->date = $date;
        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): CreateForecastDto
    {
        $this->city = $city;
        return $this;
    }

    public function getTemp(): float
    {
        return $this->temp;
    }

    public function setTemp(float $temp): CreateForecastDto
    {
        $this->temp = $temp;
        return $this;
    }
}
