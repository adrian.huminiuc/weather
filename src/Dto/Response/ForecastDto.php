<?php


namespace App\Dto\Response;

use JMS\Serializer\Annotation as JMS;

class ForecastDto
{
    /**
     * @JMS\Type("int")
     *
     * @var int
     */
    private $id;

    /**
     * @JMS\Type("string")
     *
     * @var string
     */
    private $city;

    /**
     * @JMS\Type("double")
     *
     * @var double
     */
    private $temp;

    /**
     * @JMS\Type("DateTime")
     *
     * @var \DateTime
     */
    private $date;

    public function __construct(int $id, string $city, float $temp, \DateTime $date)
    {
        $this->id = $id;
        $this->city = $city;
        $this->temp = $temp;
        $this->date = $date;
    }
}
