<?php


namespace App\Factory;


use App\Entity\Forecast;
use App\Repository\ForecastCitiesRepository;

class OpenWeatherToForecastFactory
{
    /**
     * @var ForecastCitiesRepository
     */
    private $forecastCitiesRepository;

    public function __construct(ForecastCitiesRepository $forecastCitiesRepository)
    {
        $this->forecastCitiesRepository = $forecastCitiesRepository;
    }

    public function createFromResponse(array $openWeatherResponse): array
    {
        if (!$city = $this->forecastCitiesRepository->findOneBy(['name' => $openWeatherResponse['city']['name']])) {
            throw new \InvalidArgumentException('Cannot find city' . $openWeatherResponse['city']['name']);
        }

        $results = [];
        foreach ($openWeatherResponse['list'] as $forecast) {
            $results[] = (new Forecast())
                ->setDate(new \DateTime($forecast['dt_txt']))
                ->setCity($city)
                ->setTemperature($forecast['main']['temp']);
        }

        return $results;
    }
}
