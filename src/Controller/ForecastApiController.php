<?php


namespace App\Controller;

use App\Services\WeatherService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ForecastApiController
{
    /**
     * @var WeatherService
     */
    private $weatherService;

    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    /**
     * @Route("/forecast/{city}", methods={"GET"})
     */
    public function getLastDaysForecastAction(string $city): JsonResponse
    {
        return new JsonResponse(['average_temperature' => $this->weatherService->fetch($city)]);
    }
}
