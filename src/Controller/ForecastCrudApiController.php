<?php


namespace App\Controller;


use App\Dto\Request\CreateForecastDto;
use App\Mapper\ForecastCreateDtoToEntityMapper;
use App\Mapper\ForecastEntityToDtoResponseMapper;
use App\Repository\ForecastRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ForecastCrudApiController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ForecastRepository
     */
    private $forecastRepository;

    /**
     * @var ForecastCreateDtoToEntityMapper
     */
    private $forecastDtoToEntityMapper;

    /**
     * @var ForecastEntityToDtoResponseMapper
     */
    private $entityToDtoMapper;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        ForecastRepository $forecastRepository,
        ForecastCreateDtoToEntityMapper $forecastDtoToEntityMapper,
        ForecastEntityToDtoResponseMapper $entityToDtoMapper
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->forecastRepository = $forecastRepository;
        $this->forecastDtoToEntityMapper = $forecastDtoToEntityMapper;
        $this->entityToDtoMapper = $entityToDtoMapper;
    }

    /**
     * @Route("/forecast", methods={"POST"})
     */
    public function createAction(Request $request): JsonResponse
    {
        try {
            $dto = $this->serializer->deserialize($request->getContent(), CreateForecastDto::class, 'json');
        } catch (\Throwable $exception) {
            return new JsonResponse(['error' => $exception->getMessage(), 'content' => $request->getContent()], 400);
        }

        $errors = $this->validator->validate($dto);
        if (count($errors) > 0) {
            return new JsonResponse(['error' => 'Invalid request'], 400);
        }

        $forecast = $this->forecastDtoToEntityMapper->map($dto);
        $this->forecastRepository->persist($forecast);

        return new JsonResponse(
            $this->serializer->serialize($this->entityToDtoMapper->map($forecast), 'json'),
            201,
            [],
            true
        );
    }
}
