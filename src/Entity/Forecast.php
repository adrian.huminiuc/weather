<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     indexes={@ORM\Index(name="city_date", columns={"city_id", "date"})},
 *     uniqueConstraints={
 *     @ORM\UniqueConstraint(name="city_date", columns={"city_id", "date"})
 * })
 */
class Forecast
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="decimal", precision=2)
     */
    private $temperature;

    /**
     * @ORM\ManyToOne(targetEntity="ForecastCities")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    public function getId()
    {
        return $this->id;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function setTemperature($temperature): self
    {
        $this->temperature = $temperature;
        return $this;
    }

    public function getCity(): ForecastCities
    {
        return $this->city;
    }

    public function setCity($city): self
    {
        $this->city = $city;
        return $this;
    }
}
