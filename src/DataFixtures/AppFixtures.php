<?php

namespace App\DataFixtures;

use App\Entity\ForecastCities;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // some test data
        $cities = [
            'Berlin,Berlin,3406000,DE',
            'Stuttgart,Baden-Württemberg,2944700,DE',
            'Frankfurt,Hesse,2895000,DE',
            'Mannheim,Baden-Württemberg,2362000,DE',
            'Hamburg,Hamburg,1757000,DE',
            'Essen,North Rhine-Westphalia,1742135,DE',
            'Duisburg,North Rhine-Westphalia,1276757,DE',
            'Munich,Bavaria,1275000,DE',
            'Düsseldorf,North Rhine-Westphalia,1220000,DE',
            'Cologne,North Rhine-Westphalia,1004000,DE',
            'Wuppertal,North Rhine-Westphalia,776525,DE',
            'Saarbrücken,Saarland,770001,DE',
            'Marienberg,Bavaria,737304,DE',
            'Bremen,Bremen,724909,DE',
            'Hannover,Lower Saxony,722490,DE',
            'Bonn,North Rhine-Westphalia,680543,DE',
            'Dresden,Saxony,617515,DE',
            'Wiesbaden,Hesse,617126,DE',
            'Dortmund,North Rhine-Westphalia,588462,DE',
            'Leipzig,Saxony,542529,DE',
        ];

        foreach ($cities as $cityString) {
            [$name, $state, $population, $country] = explode(',', $cityString);
            $forecast = new ForecastCities(
                $name,
                $state,
                $country,
                $population
            );

            $manager->persist($forecast);
        }
        $manager->flush();
    }
}
