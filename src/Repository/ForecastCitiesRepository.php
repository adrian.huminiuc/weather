<?php


namespace App\Repository;


use App\Entity\ForecastCities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ForecastCitiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForecastCities::class);
    }

    /**
     * @return ForecastCities[]
     */
    public function getCitiesByPopulation(string $iso2Country, int $limit = 5)
    {
        return $this->createQueryBuilder('c')
            ->where('c.country = :country')
            ->setParameter('country', $iso2Country)
            ->orderBy('c.population', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
