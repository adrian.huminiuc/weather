<?php


namespace App\Repository;

use App\Entity\Forecast;
use App\Entity\ForecastCities;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ForecastRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Forecast::class);
    }

    public function getAverageThreeDayTemperature(ForecastCities $city, \DateTime $futureDate): float
    {
        return $this->createQueryBuilder('fc')
            ->select('AVG(fc.temperature) as temperature')
            ->where('fc.date BETWEEN :now AND :future')
            ->andWhere('fc.city = :city')
            ->setParameter('now', new \DateTime('today midnight'))
            ->setParameter('future', $futureDate)
            ->setParameter('city', $city)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function persistBulk(array $forecasts): void
    {
        foreach ($forecasts as $forecast) {
            if(!$forecast instanceof Forecast) {
                throw new \InvalidArgumentException('Invalid forecast type!');
            }

            if ($this->findOneBy(['city' => $forecast->getCity(), 'date' => $forecast->getDate()])) {
                continue;
            }

            $this->_em->persist($forecast);
        }

        $this->_em->flush();
    }

    public function persist(Forecast $forecast): Forecast
    {
        $this->_em->persist($forecast);
        $this->_em->flush();

        return $forecast;
    }
}
